package com.clp.controller;

import com.clp.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

@RequestMapping("/user")
@Controller
public class UserInfoController {

    @Autowired
    UserInfoService userInfoService;

    @RequestMapping("/list")
    public ModelAndView userInfoIndex() throws IOException {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("userInfoIndex");
        mv.addObject("userList", userInfoService.getUserInfo());

        return mv;
    }

    @RequestMapping("/save")
    @ResponseBody
    public String saveUserInfo() throws IOException {

        userInfoService.saveUserInfo();
        return "保存成功！";
    }
}
