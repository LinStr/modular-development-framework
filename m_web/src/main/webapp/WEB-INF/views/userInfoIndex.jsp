<%--
  Created by IntelliJ IDEA.
  User: Wxg
  Date: 2021/4/8
  Time: 20:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
用户信息：<br />
<table border="1">
    <thead>
        <th>序号</th>
        <th>用户名</th>
        <th>密码</th>
        <th>创建日期</th>
    </thead>
    <tbody>
    <c:forEach items="${userList}" var="user" varStatus="st">
        <tr>
            <td>${st.count}</td>
            <td>${user.userName}</td>
            <td>${user.userPwd}</td>
            <td>${user.createDate}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
