package com.clp.service;

import com.clp.entity.UserInfo;

import java.util.List;

public interface UserInfoService {

    List<UserInfo> getUserInfo();
    void saveUserInfo();
}
