package com.clp.mapper;

import com.clp.entity.UserInfo;

import java.util.List;

public interface UserInfoMapper {
    List<UserInfo> getAllUserInfo();

    void saveUserInfo(UserInfo userInfo);
}
