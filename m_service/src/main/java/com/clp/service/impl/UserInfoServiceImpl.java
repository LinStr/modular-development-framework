package com.clp.service.impl;

import com.clp.entity.UserInfo;
import com.clp.mapper.UserInfoMapper;
import com.clp.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    UserInfoMapper userInfoMapper;

    @Override
    public List<UserInfo> getUserInfo() {

        List<UserInfo>  lst = userInfoMapper.getAllUserInfo();

        return lst;
    }

    @Transactional
    @Override
    public void saveUserInfo() {

        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(988);
        userInfo.setUserName("wxgTest");
        userInfo.setUserPwd("888888");
        userInfo.setCreateDate(new Date());

        userInfoMapper.saveUserInfo(userInfo);

        System.out.println("插入成功！");
    }
}
